import Vue from 'vue'

const EventBus = new Vue({
  created () {
    window.addEventListener('resize', this.onWindowResize)
  },
  beforeDestroy () {
    window.removeEventListener('resize', this.onWindowResize)
  },
  methods: {
    onWindowResize () {
      this.$emit('onWindowResize', window.innerWidth, window.innerHeight)
    }
  }
})

export default EventBus